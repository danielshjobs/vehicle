FROM python:3.12-slim-bullseye

LABEL maintainer="Daniel Shmuel"

# Set working directory
WORKDIR /app

# Install OS dependencies
RUN apt-get update \
    && apt-get install -y --no-install-recommends


# Make sure we are using latest pip
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools

# Copy requirements.txt
COPY requirements.txt .

# Install dependencies
RUN pip3 install -r requirements.txt


# Copy source code
COPY /app /app
EXPOSE 8080
CMD ["python", "app.py"]