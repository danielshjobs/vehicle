# Vehicle Micro service - provides a micro service to create vehicle events


## Getting started
At a high level, the main purpose of a vehicle microservice is to process new vehicle arrival files, where each vehicle file will be stored in a mongodb collection.

The vehicle will be responsible, that all entry events will be entered in the correct format according to the collection structure. In addition, the microservice will be responsible for several capabilities such as (schema, API, validation, external configuration and more...) to ensure non-functional requirements such as: scalability, availability, reliability we have adopted K8S capabilities such as: replication and automatic scaling of EKS.



## What Vehicle micro service includes
   - Swagger interface to specify our resources and instructions how to invoke them.
   - Vehicle app includes popular layers like (entity, controller, unit test, Service, error handler...) 
   - Vehicle API includes variety of artifacts such as: (Flask , Pymongo, waiteress, Swagger interface..)
   - Helm 3 - To mange our K8S types make it easy to publish our app.
   - Automation of CICD process - A GITHUBACTION process to run our CICD processing. 
   


## To install
   - Interpreter 3.12 
   - Install VENV
   - PIP 3
   - Recomended IDE - PYCHARM
   - Helm 3

## Run the flask app 
   - Click Run in the IDE.

## Run the worker service  
   - cd app
   - Please run this command - python launcher.py 0-10 #or any other range

## Run Tests
   - python -m unittest discover -s ./com/mobileye/test -p vehicle_test.py
  

## To deploy and build docker image
   - There is a Docker file to compress our app to docker image.
   - To deploy vehicle app we are going to use on Helm 3 by the vehicle chart has been attached to vehicle project.
   - To test & debug the chart please run the command - helm install %releaseName% %path%/%chartPath% --dry-run --debug -f %path%/%valuePath%.
   - To deploy vehicle chart there is a CICD processing based on GITHUB-ACTION for full automation illustration to deploy our app to EKS. for your convinience, The CICD process located in .github/workflows path.


## Rest API  - Endpoints details:
   - Get   /vehicle - To retrieve all our vehicle records.
   - Post  /vehicle - To add a new vehicles to vehicle collection includes vehicle status.


##Recommendation:
   - In case our queries will be increasing it will be better to manage them in repository pattern.   
   
     For your convinience, please use on swagger-ui to invoke the api below vehicle swagger path:
   -  http://localhost:8080/swagger/


