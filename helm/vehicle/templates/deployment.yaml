kind: Deployment
apiVersion: apps/v1
metadata:
  name: {{ .Chart.Name }}
  namespace: {{ .Values.namespace }}
  labels:
    app: {{ .Chart.Name }}
    release: {{ .Release.Name }}
spec:
  replicas: {{ .Values.replicaCount }}
  selector:
    matchLabels:
      app: {{ .Chart.Name }}
      release: {{ .Release.Name }}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxUnavailable: 1
      maxSurge: 1
  template:
    metadata:
      annotations:
        linkerd.io/inject: enabled
        checksum/config: {{ include (print $.Template.BasePath "/configmap.yaml") . | sha256sum }}
        checksum/secret: {{ include (print $.Template.BasePath "/secret.yaml") . | sha256sum }}
      labels:
        app: {{ .Chart.Name }}
        release: {{ .Release.Name }}
    spec:
      containers:
        - name: {{ .Chart.Name }}
          image: {{ .Values.image.repository }}:{{ .Values.image.tag }}
          ports:
            - containerPort: {{ .Values.service.targetport }}
          volumeMounts:
            - mountPath: /static
              name: cache-volume
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          livenessProbe:
            httpGet:
              port: {{ .Values.service.targetport }}
              path: /actuator/health/liveness
            initialDelaySeconds: 130
            timeoutSeconds: 10
          readinessProbe:
            httpGet:
              port: {{ .Values.service.targetport }}
              path: /actuator/health/readiness
            initialDelaySeconds: 60
            timeoutSeconds: 10
          envFrom:
            - secretRef:
                name: {{ .Chart.Name }}-secret

            - configMapRef:
                name: {{ .Chart.Name }}-configmap
          resources:
            requests:
              memory: "{{ .Values.resources.requests.memory }}"
              cpu: "{{ .Values.resources.requests.cpu }}"
            limits:
              memory: "{{ .Values.resources.limits.memory }}"
              cpu: "{{ .Values.resources.limits.cpu }}"
      volumes:
        - name: cache-volume
          emptyDir:
            sizeLimit: 128Mi
      restartPolicy: Always
      terminationGracePeriodSeconds: 10
      dnsPolicy: ClusterFirst