import logging
import sys
from com.mobileye.service import vehicle_worker_service as vehicle_worker

LOGGER = logging.getLogger("launcher")
logging.basicConfig(level=logging.INFO)

def main():
    LOGGER.info("Starts processing bulks of files")
    file_range = sys.argv[1]
    inputs = [0, 2, 4, 6, 8]
    vehicle_worker.run(file_range, inputs)


if __name__ == "__main__":
    main()
