class BadRequest(Exception):
    def __init__(self, message, status=400, payload=None):
        self.message = message
        self.status = status
        self.payload = payload




class ServerError(Exception):
    def __init__(self, message, status=500, payload=None):
        self.message = message
        self.status = status
        self.payload = payload



class NotFound(Exception):
    def __init__(self, message, status=404, payload=None):
        self.message = message
        self.status = status
        self.payload = payload