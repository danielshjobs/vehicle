import os

#Env for DB
mongoDatabase = os.getenv('mongoDatabase', 'mobileye')
collection = os.getenv('collection', 'vehicle')
mongoUserName = os.getenv('mongoUserName', 'daniel')
mongodbPassword = os.getenv('mongodbPassword', 'A123456a!')
filePath = os.getenv('filePath', 'static')
mongoHost = os.getenv('mongoHost', 'cluster0.silfm.mongodb.net/mobileye?retryWrites=true&w=majority&appName=Cluster0')
uri = 'mongodb+srv://'+str(mongoUserName)+':'+str(mongodbPassword)+'@'+str(mongoHost)
serverPort = os.getenv('serverPort', 8080)