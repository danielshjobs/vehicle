from pymongo import MongoClient
from com.mobileye.config import env_config as env

cluster = MongoClient(env.uri)
db = cluster[env.mongoDatabase]
collection = db[env.collection]
