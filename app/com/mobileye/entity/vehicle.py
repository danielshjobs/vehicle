from json import JSONEncoder
from collections import namedtuple


class Detection:
    def __init__(self, type: str, value: int):
        self.type = type
        self.value = value
class Status:
    def __init__(self, report_time: str, status_name: str):
        self.report_time = report_time
        self.status_name = status_name

class Vehicle:
    def __init__(self, id, vehicle_id, detection_time, detections: list):
        self.id = id
        self.vehicle_id = vehicle_id
        self.detection_time = detection_time
        self.detections = detections

    def set_status(self, status: Status):
        self.status = status



