import multiprocessing
from time import sleep
import json
from functools import partial
from com.mobileye.entity import vehicle
from com.mobileye.config import mongo_config as mongo
import logging

LOGGER = logging.getLogger("vehicle-worker")

def read_static_file(filepath: str):
    with open(filepath) as json_file:
        data = json.load(json_file)
        return data

def map_dict_to_vehicle(vehicle_data):
    list_vehicle = []
    for vehicle_item in vehicle_data:
        list_vehicle.append(vehicle.Vehicle(vehicle_item['_id'], vehicle_item['vehicle_id'], vehicle_item['detection_time'], vehicle_item['detections']))
    return list_vehicle

def map_status_vehicle_to_vehicle(vehicles, vehicle_status):
    for vehicle_item in vehicle_status:
        for vehicle_item_obj in vehicles:
            if(vehicle_item['vehicle_id'] == vehicle_item_obj.vehicle_id):
                status = vehicle.Status(vehicle_item['report_time'], vehicle_item['status'])
                vehicle_item_obj.set_status(status)
    return vehicles

def map_vehicles_to_bson(vehicles):
    list_doc = []
    doc = {}
    for vehicle_item in vehicles:
        doc = {"_id": vehicle_item.id, "vehicle_id": vehicle_item.vehicle_id, "detection_time": vehicle_item.detection_time,
               "detections": vehicle_item.detections, "status": {"report_time": vehicle_item.status.report_time, "status_name": vehicle_item.status.status_name}}
        list_doc.append(doc)
    return list_doc
def save(partition, page):
    vehicles_doc = []
    LOGGER.info('Running save method to store our vehicles')
    for processing in range(page, page+2):
        LOGGER.info(f'Running save method to store our vehicles on page: {processing}')
        vehicle_data = read_static_file(f'static/{partition}/vehicle{processing}.json')
        vehicles = map_dict_to_vehicle(vehicle_data['objects_detection_events'])
        vehicle_status = read_static_file(f'static/{partition}/vehicle_status{processing}.json')
        vehicles = map_status_vehicle_to_vehicle(vehicles, vehicle_status['vehicle_status'])
        vehicles_doc = map_vehicles_to_bson(vehicles)
        sleep(10)
        mongo.collection.insert_many(vehicles_doc)
        LOGGER.info(f"save method has been running successfully and loaded our data:  {vehicles_doc}")
    return vehicles_doc

def run(partition, inputs):
    pool = multiprocessing.Pool()
    pool = multiprocessing.Pool(processes=5)
    results = pool.map(partial(save, partition), inputs)
    pool.close()
    pool.join()
    LOGGER.info(f"all our doc have been stored successfully:: {results}")