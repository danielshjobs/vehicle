import json
from com.mobileye.entity import vehicle
from com.mobileye.config import mongo_config as mongo
import logging

LOGGER = logging.getLogger("vehicle-service")
def read_static_file(filepath: str):
    with open(filepath) as json_file:
        data = json.load(json_file)
        return data

def map_dict_to_vehicle(vehicle_data):
    list_vehicle = []
    for vehicle_item in vehicle_data:
        list_vehicle.append(vehicle.Vehicle(vehicle_item['_id'], vehicle_item['vehicle_id'], vehicle_item['detection_time'], vehicle_item['detections']))
    return list_vehicle

def map_status_vehicle_to_vehicle(vehicles, vehicle_status):
    for vehicle_item in vehicle_status:
        for vehicle_item_obj in vehicles:
            if(vehicle_item['vehicle_id'] == vehicle_item_obj.vehicle_id):
                status = vehicle.Status(vehicle_item['report_time'], vehicle_item['status'])
                vehicle_item_obj.set_status(status)
    return vehicles

def map_vehicles_to_bson(vehicles):
    list_doc = []
    doc = {}
    for vehicle_item in vehicles:
        doc = {"_id": vehicle_item.id, "vehicle_id": vehicle_item.vehicle_id, "detection_time": vehicle_item.detection_time,
               "detections": vehicle_item.detections, "status": {"report_time": vehicle_item.status.report_time, "status_name": vehicle_item.status.status_name}}
        list_doc.append(doc)
    return list_doc
def save(name, status):
    LOGGER.info('Running save method to store our vehicles')
    vehicle_data = read_static_file(f'static/{name}')
    vehicles = map_dict_to_vehicle(vehicle_data['objects_detection_events'])
    vehicle_status = read_static_file(f'static/{status}')
    vehicles = map_status_vehicle_to_vehicle(vehicles, vehicle_status['vehicle_status'])
    vehicles_doc = map_vehicles_to_bson(vehicles)
    mongo.collection.insert_many(vehicles_doc)
    LOGGER.info(f"save method has been running successfully and loaded our data:  {vehicles_doc}")
    return vehicles_doc


def find_all():
    vehicles_doc = []
    LOGGER.info('Running findAll method to retrieve all our vehicles')
    for item in mongo.collection.find():
        vehicles_doc.append(item)
    LOGGER.info(f"find_all method has been running successfully and retrieved all our data:  {vehicles_doc}")
    return vehicles_doc
