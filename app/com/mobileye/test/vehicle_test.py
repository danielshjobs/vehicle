import json
import sys
import requests
import unittest
from unittest.mock import patch, Mock

def post_vehicle(files):
    byte_length = str(sys.getsizeof(files))
    headers = {'Content-Type': "application/json", 'Content-Length': byte_length}
    response = requests.post("http://localhost:8080/vehicle",
                             data=json.dumps(files),
                             headers=headers)
    return response.json()

class TestVehicleData(unittest.TestCase):
    @patch('requests.post')
    def test_post_vehicle(self, mock_post):
        mock_response = Mock()
        response_dict = {"_id": "ebab5f787798416fb2b8afc1340d7a4e-5", "vehicle_id": "ebae3f787798416fb2b8afc1340d7a6d", "detection_time": "2022-06-05T21:11:35.567Z"}
        mock_response.json.return_value = response_dict
        mock_post.return_value = mock_response
        request_data = {"fileName": "vehicle_07_03_2024.json", "filename2": "vehicle_status_07_03_2024.json"}
        vehicle_data = post_vehicle(request_data)
        mock_post.assert_called_with("http://localhost:8080/vehicle", data=json.dumps(request_data), headers={'Content-Type': 'application/json', 'Content-Length': '184'})
        self.assertEqual(vehicle_data, response_dict)
