
from flask import Flask, jsonify, request
from flask_swagger_ui import get_swaggerui_blueprint
from waitress import serve
from com.mobileye.errorhandler import web_error as ex
from com.mobileye.service import vehicle_service
from com.mobileye.config import env_config as env
import logging

logging.basicConfig(level=logging.DEBUG)

app = Flask(__name__)
SWAGGER_URL = "/swagger"
API_URL = "/static/swagger.json"

swagger_ui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': 'Access API'
    }
)
app.register_blueprint(swagger_ui_blueprint, url_prefix=SWAGGER_URL)

@app.route("/vehicle")
def find_vehicle():
    vehicles = vehicle_service.find_all()
    return jsonify(vehicles)

@app.route("/vehicle",methods=["POST"])
def add_vehicle():
    data = request.get_json()
    file1 = data.get("filename", "vehicle_07_03_2024.json")
    file2 = data.get("filename2", "vehicle_status_07_03_2024.json")
    app.logger.info("loading our files")

    if not data or data is None or data == "":
        raise ex.BadRequest('Body cannot be empty or invalid', 400, {'ext': 1})

    vehicles = vehicle_service.save(file1, file2)
    message = f"User {file1} received access to server {file2}"
    return jsonify(
        vehicles[0]
    )

@app.errorhandler(ex.BadRequest)
def handleBadRequest(error):
    payload = dict(error.payload or ())
    payload['status'] = error.status
    payload['message'] = error.message
    return jsonify(payload), 400


@app.errorhandler(ex.NotFound)
def handleNotFoundRequest(error):
    payload = dict(error.payload or ())
    payload['status'] = error.status
    payload['message'] = error.message
    return jsonify(payload), 404


@app.errorhandler(ex.ServerError)
def handleInteralRequest(error):
    payload = dict(error.payload or ())
    payload['status'] = error.status
    payload['message'] = error.message
    return jsonify(payload), 500


if __name__ == "__main__":
    #app.run(host="0.0.0.0", port=8080)
    serve(app, host='0.0.0.0', port=env.serverPort)